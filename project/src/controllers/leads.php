<?php
namespace Controller;

use Silex\Application;
use Silex\Api\ControllerProviderInterface;
USE Silex\Application\TwigTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use MongoClient;
use MongoDate;
use MongoId;

class leads implements ControllerProviderInterface {

  //-------------- Routing ------------------------------------------
  public function connect(Application $app) {
   
    $controllers = $app['controllers_factory'];
    $controllers->get('/', 'Controller\leads::lists')->method('GET');
    $controllers->get('/{leadid}', 'Controller\leads::read')->method('GET');
    $controllers->post('/create', 'Controller\leads::create')->method('POST');
    $controllers->put('/update/{leadid}', 'Controller\leads::update')->method('PUT');

    return $controllers;
  }


  //-------------- Find All Leads' Data ------------------------------------------
  public function lists(Application $app, Request $request){

    // GET params
    $params = $request->query->all();

    // For debug
    if(!isset($params['per_page'])){
      $params['per_page'] = 3;
    }
    if(!isset($params['page'])){
      $params['page'] = 1;
    }

    // connect
    $m = new MongoClient('mongodb://mongo:27017');
    // select a database
    $db = $m->imoney;

    // select a collection (analogous to a relational database's table)
    $collection = $db->leads;
    $myArray = array() ;

    if($params['page'] > 1){
      $params['page'] = ($params['page'] - 1) * $params['per_page'];
    }
    else{
      $params['page'] = 0;
    }

    // find everything in the collection
    $cursor = $collection->find()->limit($params['per_page'])->skip($params['page'])->sort(array("updated_at" => -1));;
    $dbCount = $collection->count();
    $rows = iterator_to_array($cursor);
    
    return  $app->json(["rows" =>  $rows, "count" => $dbCount]);

  }

  //-------------- Create One New Lead ------------------------------------------
  public function create(Application $app, Request $request){
    // GET params
    if (strpos($request->headers->get('Content-Type'), 'application/json') === 0) {
        $data = json_decode($request->getContent(), true);
        $request->request->replace(is_array($data) ? $data : array());
    }
    $params = $request->request->all();
    $params['created_at'] = new MongoDate();
    $params['updated_at'] = new MongoDate();

     // connect
    $m = new MongoClient('mongodb://mongo:27017');
    // select a database
    $db = $m->imoney;

    // select a collection (analogous to a relational database's table)
    $collection = $db->leads;
    $res = array("err" => false);
    try{
      $document = $collection->insert($params, array("w" => 1, "j" => true,));

    } catch(MongoCursorException $e){
       $res['err'] = true;
    }
    

    return $app->json($res);
  }

  //-------------- Find One Lead's Data ------------------------------------------
  public function read(Application $app, Request $request, $leadid){
    // $params = $request->attributes;
    // connect
    $m = new MongoClient('mongodb://mongo:27017');
    // select a database
    $db = $m->imoney;

    // select a collection (analogous to a relational database's table)
    $collection = $db->leads;
    $document = $collection->findOne(array("_id" => new MongoId($leadid)));
  
    return $app->json($document);

  }

  //-------------- Update One Lead's Data ------------------------------------------
  public function update(Application $app, Request $request, $leadid){
    if (strpos($request->headers->get('Content-Type'), 'application/json') === 0) {
        $data = json_decode($request->getContent(), true);
        $request->request->replace(is_array($data) ? $data : array());
    }
    $params = $request->request->all();
    unset($params['_id']);
    $params['updated_at'] = new MongoDate();

    // connect
    $m = new MongoClient('mongodb://mongo:27017');
    // select a database
    $db = $m->imoney;

    // select a collection (analogous to a relational database's table)
    $collection = $db->leads;
    $document = $collection->update(array("_id" => new MongoId($leadid)), $params);
    // var_dump($params);
    return $app->json($document);

  }
}