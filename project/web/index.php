<?php

require_once __DIR__.'/../vendor/autoload.php';
USE Silex\Application\TwigTrait;
use Symfony\Component\HttpFoundation\Request;


$app = new Silex\Application();
$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__.'/../src/views',
));

$app['debug'] = true;

$app->get('/', function() use($app){
	return $app['twig']->render('index.html');
});

$app->mount('/api/leads', new Controller\leads());

Request::enableHttpMethodParameterOverride();
$app->run();