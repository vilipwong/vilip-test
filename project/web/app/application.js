'use strict';

angular.module(AppConfig.appName, AppConfig.appModuleDependencies);

angular.element(document).ready(function () {
    // Init the app
    angular.bootstrap(document, [AppConfig.appName]);
    
});
