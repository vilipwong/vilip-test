'use strict';

var AppConfig = (function () {
    var appName = 'iMoney';
    var appModuleDependencies = ['ui.router'];

    var registerModule = function (moduleName, dependencies) {
        angular.module(moduleName, dependencies || []);

        angular.module(appName).requires.push(moduleName);
    };

    return {
        appName: appName,
        appModuleDependencies: appModuleDependencies,
        registerModule: registerModule
    };

})();