'use strict';

angular.module('core').factory('Flash', ['$rootScope', '$sce',
    function ($rootScope, $sce) {
        var alerts = [];
        var count = 0;

        // Convert string into trusted HTML
        var setTrustedHTML = function (message) {
            return $sce.trustAsHtml("<span>"+message+"</span>");
        };

        $rootScope.$on('$stateChangeSuccess', function () {
            // reset alerts
            alerts = [];
        });

        return {
            setAlerts: function (alert, reset) {
                //if (typeof reset === 'undefined') reset = true;
                //if (reset) alerts = [];
                //alert.message = setTrustedHTML(alert.message);
                //alerts = alerts.concat(alert);
                toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": false,
                    "positionClass": "toast-top-right",
                    "onclick": null,
                    "showDuration": "1000",
                    "hideDuration": "1000",
                    "timeOut": "0",
                    "extendedTimeOut": "0",
                    "showEasing": "swing",
                    "tapToDismiss": false,
                    "hideEasing": "linear",
                    "showMethod": "slideDown",
                    "hideMethod": "slideUp"
                };

                if(alert.type === 'danger'){
                    toastr["error"](alert.message);
                }
                else{
                    toastr[alert.type](alert.message);
                }

                if(count === 0){
                    toastr.options.newestOnTop = true;
                    toastr.options.closeButton = false;
                    toastr.options.onclick = function(){toastr.clear(); count = 0;};
                    toastr["info"]("Click me to clear all notifications");
                }
                count++;
            },
            setAlertsWithLink: function (alert, reset) {
                if (typeof reset === 'undefined') reset = true;
                if (reset) alerts = [];
                var HTMLText = alert.message;
                var HTMLLink = HTMLText.link(alert.baseUrl + alert.link + alert.parameter);
                alert.message = setTrustedHTML(HTMLLink);
                alerts = alerts.concat(alert);
            },
            getAlerts: function () {
                return alerts;
            },
            closeAlert: function (index) {
                alerts.splice(index, 1);
            },
            resetAlerts: function () {
                alerts = [];
            }
        };
    }
])
    .factory('FlashAfterRedirect', ['$rootScope' ,function ($rootScope) { // set message in queue and to be retrieved and flash after redirect
        var queue = [], currentMessage = null;
        $rootScope.$on('$stateChangeSuccess', function() {
            if (queue.length > 0){
                currentMessage = queue.shift();
            }
            else{
                currentMessage = null;
            }
        });

        return {
            set: function(message, type) {
                queue.push({message: message, type: type});
            },
            get: function() {
                return currentMessage;
            }
        };
    }]);
