'use strict';

// Pagination service
angular.module('core').factory('Pagination', [
    function () {

        var Pagination = function (options) {
            if (typeof options === 'undefined') options = {};
            this.attributes = {
                count: 0,
                page: 1,
                perPage: 10,
                maxSize: 5
            };
            this.setPagination(options);
        };

        /**
         * Set Pagination values and calculate to and from values.
         * @param pagination
         * @param {Number} [pagination.count]
         * @param {Number} [pagination.page]
         * @param {Number} [pagination.perPage]
         * @param {Number} [pagination.maxSize]
         */
        Pagination.prototype.setPagination = function (pagination) {
            if (typeof pagination.count !== 'undefined') this.attributes.count = pagination.count;
            if (typeof pagination.page !== 'undefined') this.attributes.page = pagination.page;
            if (typeof pagination.perPage !== 'undefined') this.attributes.perPage = pagination.perPage;
            if (typeof pagination.maxSize !== 'undefined') this.attributes.maxSize = pagination.maxSize;

            this.attributes.from = ((this.attributes.page - 1) * this.attributes.perPage) + 1;
            var to = ((this.attributes.page - 1) * this.attributes.perPage) + this.attributes.perPage;
            if (to > this.attributes.count) {
                this.attributes.to = this.attributes.count;
            } else {
                this.attributes.to = to;
            }
            return this.attributes;
        };

        /**
         * Move to next page
         */
        Pagination.prototype.nextPage = function () {
            if ((this.attributes.page + 1) * this.attributes.perPage <= (this.attributes.count + this.attributes.perPage))
                this.attributes.page += 1;
        };

        /**
         * Move to previous page
         */
        Pagination.prototype.previousPage = function () {
            if ((this.attributes.page - 1) > 0)
                this.attributes.page -= 1;
        };

        return Pagination;
    }
]);
