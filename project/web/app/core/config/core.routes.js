'use strict';

angular.module('core').config(['$locationProvider', '$urlRouterProvider', '$stateProvider',
    function ($locationProvider, $urlRouterProvider, $stateProvider) {

    

        // setup default routes
        $urlRouterProvider.otherwise('/');

        $locationProvider.html5Mode({
            enabled: true,
            requireBase: true
        });

        $stateProvider
            .state('home', {
                url: '/',
                templateUrl: '/app/core/views/home.html'
            });
    }
]);
