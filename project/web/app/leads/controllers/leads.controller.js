'use strict';

angular.module('leads')
    .controller('LeadsController', ['$window', '$location', '$http', '$state',
    	'Pagination', 'Flash', 'FlashAfterRedirect', 
        function( $window, $location, $http, $state,
        	Pagination, Flash, FlashAfterRedirect) {
            var scope = this;

           	// Pagination initial settings
            scope.pagination = new Pagination();
            scope.pagination.attributes.perPage = 5;

            // Make API Friendly Query
            scope.query = {
                page: scope.pagination.attributes.page,
                per_page: scope.pagination.attributes.perPage
            };

            /**
             * Handle Page Changes
             */
            scope.pageChanged = function () {
                scope.query.page = scope.pagination.attributes.page;
                scope.fetch();
            };

            scope.isFlashMsg = function () {
                if(FlashAfterRedirect.get()){
                    Flash.setAlerts({type: FlashAfterRedirect.get().type, message: FlashAfterRedirect.get().message});
                }
            };

            scope.fetch = function(){
            	$http.get('/api/leads', {params: scope.query})
                    .then(function (response) {
                        scope.leads = response.data.rows;
                        
	                    scope.leadsCount = response.data.count;
	                    scope.pagination.setPagination({count: response.data.count});

	                    
                    }, function(err){
                    	console.log(err);
                    	console.log('eads err');
                    });
            };
        }
    ])
	.controller('LeadController', ['$window', '$location', '$http', '$state', '$stateParams', 
    	'Pagination', 'Flash', 'FlashAfterRedirect', 
        function( $window, $location, $http, $state, $stateParams,
        	Pagination, Flash, FlashAfterRedirect) {
            var scope = this;

            scope.lead = {};

            scope.openCalendar = function (calendar) {
                scope[calendar + 'Opened'] = true;
            };

         	/**
             * Form Error Handling
             */
            scope.hasError = function (field, validations) {
                var hasError = false;
                validations = [].concat(validations);

                validations.forEach(function (validation) {
                    if ((scope.form[field].$dirty && scope.form[field].$error[validation]) ||
                        (scope.submitted && scope.form[field].$error[validation])) {
                        hasError = true;
                    }
                });
                return hasError;
            };

            scope.findOne = function(){
                $http.get('/api/leads/'+ $stateParams.id)
                    .then(function (response) {
                        scope.lead = response.data;
                        scope.lead.dob = new Date(response.data.dob);
                        
                    }, function(err){
                        console.log(err);
                        console.log('eads err');
                    });
            };

            scope.create = function () {
                scope.submitted = true;
                if (scope.form.$valid) {
                	$http.post('/api/leads/create', scope.lead)
                		.then(function(response){
                            if(typeof response.data  === 'string'){
                                Flash.setAlerts({
                                    type: 'danger', message: 'Error creating data.',
                                    icon: 'fa fa-exclamation-circle'
                                });
                            }
                            else{
                                FlashAfterRedirect.set(scope.lead.name + ' created successfully.', 'success');
                                $location.path('/leads');
                            }
                            

                		}, function(err){
                			console.log(err);
                    	   console.log('eads err');

                		});
                } else {
                    Flash.setAlerts({type: 'danger', message: 'Please fill up the required fields.',
                        icon: 'fa fa-exclamation-circle'});
                }
            };

           

            scope.update = function(){
                scope.submitted = true;
                if (scope.form.$valid) {
                    $http.put('/api/leads/update/'+ $stateParams.id, scope.lead)
                    .then(function(response){
                        if(response.data.n == 0){
                            Flash.setAlerts({
                                type: 'danger', message: 'Error updating data.',
                                icon: 'fa fa-exclamation-circle'
                            });
                        }
                        else{
                             Flash.setAlerts({type: 'success', message: 'Updated successfully.', icon: 'fa fa-check'});
                        }

                    }, function(err){
                        console.log(err);
                        console.log('eads err');
                    })
                }
                else{
                    Flash.setAlerts({type: 'danger', message: 'Please fill up the required fields.',
                        icon: 'fa fa-exclamation-circle'});
                }
            };

        }
    ]);