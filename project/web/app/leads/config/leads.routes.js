'use strict';

angular.module('leads').config(['$stateProvider',
    function ($stateProvider) {
        
        $stateProvider
            .state('leads', {
                url: '/leads',
                templateUrl: 'app/leads/views/list-leads.html'
            })
            .state('createLead', {
                url: '/leads/create',
                templateUrl: 'app/leads/views/create-leads.html'
            })
            .state('editLead', {
                url: '/leads/:id',
                templateUrl: 'app/leads/views/edit-leads.html'
            });
    }
]);